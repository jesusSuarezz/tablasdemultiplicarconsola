// requireds
const fs = require('fs'); // Librerias nativas de nodejs
const colors = require('colors'); // son librerias que hacen otros desarrolladores
//const fs = require("../fs"); // Archivos que nosotros hacemos y estan en nuestro proyecto

let listarTabla = (base, limite = 10) => {
    
    console.log('================='.green);
    console.log(`  Tabla de ${base}`.red );
		console.log(`=================`.green);

    for (let i = 1; i <= 10; i++) {
		console.log(`${base} * ${i} : ${base * i}`);
	}
};

let crearArchivo = (base, limite=10) => {
	return new Promise((resolve, reject) => {
		if (!Number(base)) {
			reject(`El valor introducido "${base}" no es un número`);
			return;
		}

		let data = '';

		for (let i = 1; i <= limite; i++) {
			data += `${base} * ${i} : ${base * i}`;
		}

		fs.writeFile(`tablas/tabla-del-${base}-al-${limite}.txt`, data, (err) => {
			if (err) reject(err);
			else resolve(`tabla-del-${base}-al-${limite}.txt`);
		});
	});
};

// Exportamos nuestra funcion(Promise) el archivo de los modulos para utilizarla en cualquier parte de la app
module.exports = {
	crearArchivo: crearArchivo,
	listarTabla,
};
