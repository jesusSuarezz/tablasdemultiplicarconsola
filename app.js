const argv = require('./config/yargs').argv;
const colors = require('colors/safe');

const { crearArchivo, listarTabla } = require('./multiplicar/multiplicar');

let comando = argv._[0];

switch (comando) {
	case 'listar':
		console.log('Listar');
		listarTabla(argv.base, argv.limite);
		break;
	case 'crear':
		console.log('crear');
		crearArchivo(argv.base, argv.limite)
			.then((archivo) => console.log(`Archivo creado: `, colors.green(archivo)))
			.catch((error) => console.log(error));
		break;
	default:
		console.log('comando no recomocido');
		break;
}
//console.log(arvg);

//console.log("Limite", argv.limite);

//let parametro = argv[2];
//let base = parametro.split("=")[1]; // separamos el arreglo y enviamos la posicion 1
